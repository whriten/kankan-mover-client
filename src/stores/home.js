import {defineStore} from 'pinia'

import {requestHomeData} from '@/api/home'

export const useStore = defineStore('home', {
    state:()=>{
        return {
            menuList:[]
        }
    },
    actions:{
        // 更新当前顶部栏的状态
        async updateMenuList(){
            let {data:res} = await requestHomeData()
            if (res.code == 200) {
                // console.log('width',width);
                this.menuList = res.data.cate_title_array
            }
        }
    }
})