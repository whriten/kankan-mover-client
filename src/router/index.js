import {createRouter,createWebHashHistory,createWebHistory} from 'vue-router'

import Home from '@/views/Home/Home.vue'
import Search from '@/views/Search/Search.vue'
import Play from '@/views/Play/Play'

// 配置路由表
const routes = [{
    path:'/',
    component:Home,
    meta: {
        title:'快看-首页'
    }
},{
    path:'/home',
    name:'Home',
    component:Home,
    meta:{
        title:'快看-首页'
    }
},{
    path:'/search',
    name:'Search',
    component:Search,
    meta: {
        title:'快看-搜索'
    }
},{
    path:'/play',
    name:'Play',
    component:Play,
    meta:{
        title:'快看-播放'
    }
},{
    path:'/detail',
    name:'Detail',
    component:() => import('@/views/Detail/Detail.vue'),
    meta:{
        title:'详情'
    }
},{
    path:'/cate',
    name:'Cate',
    component: () => import('@/views/Cate/Cate.vue'),
    meta: {
        title:'快看-分类'
    }
},{
    path: '/:pathMatch(.*)*',
    name: 'NotFound', 
    component: () => import('@/views/error/404.vue'),
    meta: {
        title:'404',
    }
}]

const route = createRouter({
    routes,
    history:createWebHashHistory()
})

route.beforeEach((to,from,next)=>{
    document.title = to.meta.title
    console.log(to,from);
    next()
})

export default route