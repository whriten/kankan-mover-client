import { createApp } from 'vue'
document.title = process.env.VUE_APP_TITLE
import App from './App.vue'
import './util/normalize.css'

import ArcoVue from '@arco-design/web-vue'
import '@arco-design/web-vue/dist/arco.css'

import pinia from '@/stores/init'
import route from '@/router/index'
import lazyPlugin from 'vue3-lazy'

import 'animate.css'

createApp(App)
.use(ArcoVue)
.use(lazyPlugin,{
    loading:require('@/assets/images/loading.gif'), // 图片加载时默认图片
    error:require('@/assets/images/error.gif')
})
.use(route)
.use(pinia)
.mount('#app')
