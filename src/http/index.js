import axios from "axios";

export const http  =  axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    timeout:10000
})



// 请求拦截器
http.interceptors.request.use(function(config){
  return config      
},function(err){
  return  Promise.reject(err)
})


http.interceptors.response.use(function(response){
    return response    
},function(err){
    return Promise.reject(err)
})

