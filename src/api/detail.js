import {http} from '@/http/index'

// 获取详情
export const getDetail = (path) => {
    return http({
        method:'GET',
        url:`/api/detail?path=${path}`
    })
}