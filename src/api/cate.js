import {http} from '@/http/index'

/**
 * @name getCateList
 * @description 获取视频的分类
 * @param {string} path - 当前分类的地址
 * @param {number} page - 页码
*/

export const getCateList = (path, page = 1) => {
    return http({
        method: "GET",
        url: `/api/cate/detail?path=${path}&page=${page}`
    })
}