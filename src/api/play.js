import {http} from '@/http/index'


// 获取播放链接以及推荐影视
export const requestPlayerList = (path,m) => {
    return http({
        method:'GET',
        url:`/api/play?path=${path}&m=${m}`,
    })
}