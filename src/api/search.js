import {http} from '@/http/index'
import qs from 'qs'

/**
 * @name httpSearch
 * @description 获取搜索结果页
 * @param {string} wd 搜索关键词
 * @param {string} submit 搜索
 * @param {number} page 页码
 * 
*/
export const httpSearch = (wd,submit,page) => {
    return http({
        method:'POST',
        url:'/api/search?page=' + page,
        headers:{"Content-Type":"application/x-www-form-urlencoded"},
        data:qs.stringify({wd,submit})
    })
}