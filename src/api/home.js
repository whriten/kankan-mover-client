import {http} from '@/http/index'

// 请求首页的链接
export const requestHomeData = () => {
    return http({
        url:'/api/home',
        method:"GET"
    })
}